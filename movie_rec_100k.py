# -*- coding: utf-8 -*-
"""
Created on Fri Feb 07 20:43:30 2014

@author: daredavil
"""

from __future__ import division
import numpy as np
import scipy.sparse as sp
import nimfa

class HATA(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class Recommender():
    def __init__(self):
        self.W,self.H,self.method = None,None,None
    
    def read_triple(self, file_path):
        """
        returns triplets (user_id, movie_id, rating)
        """
        V = []
        n_user,n_movie = 0,0
        dc,n_record = 0,0
        with open(file_path,'r') as f_:
            for line in f_:
                user_id,movie_id,rating = map(int,line.split()[:3])
                dc+=rating
                n_record+=1
                if user_id>n_user:
                    n_user = user_id
                if movie_id>n_movie:
                    n_movie = movie_id
                V.append((user_id,movie_id,rating))
        dc/=n_record
        self.n_user = n_user
        self.n_movie = n_movie
        self.dc = dc
        return V,n_user,n_movie,dc
                
        
    def read_txt(self, file_path_or_V,n_user=None,n_movie=None,direct=False):
        if direct and (n_user==None or n_movie==None):
            raise HATA('invalid n_user or n_movie')
        X_train = sp.lil_matrix((n_user,n_movie))
        if direct:
            file_path = file_path_or_V
            with open(file_path,'r') as f_:
                for line in f_:
                    user_id,movie_id,rating = map(int,line.split()[:3])
                    X_train[user_id-1,movie_id-1] = rating
        else:
            for user_id, movie_id, rating in file_path_or_V:
                X_train[user_id-1,movie_id-1] = rating
                
        return X_train
        
        
    def preprocess(self, X_train):
        print "preprocess starts..."
        X_train = X_train.tocsr()
        maxs = [np.max(X_train[i, :].todense()) for i in xrange(X_train.shape[0])]
        now = 0
        for row in xrange(X_train.shape[0]):
            upto = X_train.indptr[row+1]
            while now < upto:
                col = X_train.indices[now]
                X_train.data[now] /= maxs[row]
                now += 1
        print "... Finished." 
        return X_train, maxs
    
    def _fit_nmf(self, X_train, y_train = None, rank = 12, max_iter=10):
        """
        Perform SNMF/R factorization on the sparse MovieLens data matrix. 
        Return basis and mixture matrices of the fitted factorization model.
        X_train : scipy.sparse.csr_matrix
        W: basis
        H: mixtures
        solves: V = W*H  with low rank W
        """
        if not isinstance(X_train, sp.csr.csr_matrix):
            X_train = X_train.tocsr()
        
        print "nmf training starts..."
        model = nimfa.mf(X_train, 
                  seed = "random_vcol", 
                  rank = rank, 
                  method = "snmf", 
                  max_iter = max_iter, 
                  initialize_only = True,
                  version = 'r',
                  eta = 1.,
                  beta = 1e-4, 
                  i_conv = 10,
                  w_min_change = 0)
        nmf = nimfa.mf_run(model)
        sparse_w, sparse_h = nmf.fit.sparseness()
        print """Stats:
            - iterations: %d
            - Euclidean distance: %5.3f
            - Sparseness basis: %5.3f, mixture: %5.3f""" % (nmf.fit.n_iter, nmf.distance(metric = 'euclidean'), sparse_w, sparse_h)
        return nmf.basis(), nmf.coef()
        
    def _fit_ssvd(self, X_train, y_train = None, rank = 12, max_iter=10,W=None,H=None,lr = 0.02, reg_lambda = 0.05):
        """
        solve = r[u,m] = dc + dc_user[u] + dc_movie[m] + W[u]*H[:,m]
        returns [W,dc_user,1], [H;1;dc_movie]
        so W*H = W[u]*H[:,m] + dc_user[u] + dc_movie[m] = r[u,m]
        """
        dc_user = np.ones(self.n_user)*0.03
        dc_movie = np.ones(self.n_movie)*0.03
        if W is None:
            W = np.ones((self.n_user,rank))*0.03
        if H is None:
            H = np.ones((rank,self.n_movie))*0.03
        
        n_iter = 0
        while n_iter<max_iter:
            rmse = 0
            for k, (user_id,movie_id,rating) in enumerate(X_train):
                pred = self.dc + dc_user[user_id-1] + dc_movie[movie_id-1] + W[user_id-1].dot(H[:,movie_id-1])
                e = rating-pred
                rmse += e**2
                dc_user[user_id-1] = dc_user[user_id-1] + lr*(e - reg_lambda*dc_user[user_id-1])
                dc_movie[movie_id-1] = dc_movie[movie_id-1] + lr*(e - reg_lambda*dc_movie[movie_id-1])
                H[:,movie_id-1] = H[:,movie_id-1] + lr*(e*W[user_id-1] - reg_lambda* H[:,movie_id-1])
                W[user_id-1] = W[user_id-1] + lr*(e*H[:,movie_id-1] - reg_lambda*W[user_id-1])                
            n_iter+=1
            #W /= np.sqrt(np.sum(W**2,axis = 0))
            print "iteration: ",n_iter, " rmse: ", np.sqrt(rmse/len(X_train))
        W = np.c_[W,dc_user,np.ones(W.shape[0])]
        H = np.vstack((H,np.ones(H.shape[1]),dc_movie))
        return W,H
        
    
    def fit(self, X_train,y_train=None, method = 'nmf', rank = 12, max_iter=10, W=None, H=None, lr = 0.02, reg_lambda = 0.05):
        self.method = method
        if method == 'nmf':
            self.W, self.H = self._fit_nmf(X_train,y_train,rank,max_iter)
        elif method == 'ssvd':
            self.W, self.H = self._fit_ssvd(X_train,y_train,rank,max_iter,W=W,H=H,lr=lr,reg_lambda=reg_lambda)
    
    def initialize_ssvd(self, V_temp, rank):
        """
        initialize W and H with nonnegative svd
        """
        W,_,H = sp.linalg.svds(V_temp,k=rank)
        W[W<0] = 0
        H[H<0] = 0
        return W,H
        
    
    def transform(self,X_test,dc = 0):
        """
        X_test is an ndarray with N*2 dimensions: [(user_id,movie_id),(user_id,movie_id),...]
        N: number of instances
        returns predicted ratings
        """
        predicted_rating = np.zeros(X_test.shape[0])
        for k,(user_id, movie_id) in enumerate(X_test):
            predicted_rating[k] = self.W[user_id-1].dot(self.H[:,movie_id-1]) + dc
        return predicted_rating
        
    def rmse(self,actual_rating,predicted_rating):
        return np.sqrt(np.mean((actual_rating-predicted_rating)**2))
    
    
if __name__ == '__main__':
    def test_nmf():
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.base'
        recommender = Recommender()
        V_train,n_user,n_movie,_ = recommender.read_triple(file_path)
        X_train = recommender.read_txt(V_train,n_user=n_user,n_movie=n_movie,direct=False)
        X_train,maxs = recommender.preprocess(X_train)        
        recommender.fit(X_train,method='nmf',rank = 12, max_iter = 1)
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.test'
        V_test,n_user,n_movie,_ = recommender.read_triple(file_path)
        V_test = np.array(V_test)
        X_test,y_test = V_test[:,:-1], V_test[:,-1]
        y_pred = recommender.transform(X_test)
        print "rmse: ", recommender.rmse(y_test,y_pred)
        
    def test_ssvd():
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.base'
        rank = 25
        recommender = Recommender()
        V_train,n_user,n_movie,dc = recommender.read_triple(file_path)
        X_train = recommender.read_txt(V_train,n_user=n_user,n_movie=n_movie,direct = False)
        W,H = recommender.initialize_ssvd(X_train.tocsr(),rank=rank)
        recommender.fit(V_train,method='ssvd',rank = rank, max_iter = 5,W=W,H=H,lr=0.025,reg_lambda=0.000001)
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.test'
        V_test,n_user,n_movie,_ = recommender.read_triple(file_path)
        V_test = np.array(V_test)
        X_test,y_test = V_test[:,:-1], V_test[:,-1]
        y_pred = recommender.transform(X_test,dc)
        print "rmse: ", recommender.rmse(y_test,y_pred)
    test_ssvd()