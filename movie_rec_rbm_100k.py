# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 16:23:39 2014

@author: daredavil
"""

from __future__ import division
import numpy as np
import scipy.sparse as sp
from sklearn.utils.extmath import logistic_sigmoid

def softmax_act(X):
    exp_X = np.exp(X)
    if X.ndim == 1:
        return exp_X/exp_X.sum()
    return exp_X/exp_X.sum(axis=0)
    
def batch_func(Y,batch_size = 100, max_rating = 5):
    #Y should be sparse
    n_user = Y.shape[0]
    beg,last = 0,0
    while last != n_user:
        last = np.min([beg + batch_size, n_user])
        Y_ = Y[beg:last].todense()
        missing_index = Y_ == 0
        yield (to_index(Y_,max_rating),missing_index,beg,last)
        beg = last
        
def batch_func_gauss(Y, batch_size=100):
    n_user = Y.shape[0]
    beg,last = 0,0
    while last != n_user:
        last = np.min([beg + batch_size, n_user])
        Y_ = Y[beg:last].todense()
        missing_index = Y_ == 0
        yield (np.array(Y_),missing_index,beg,last)
        beg = last
    
def to_index(Y, max_rating):
    Y_new = np.zeros((max_rating, Y.shape[0], Y.shape[1]))
    for k in xrange(max_rating):
        Y_new[k][Y == k+1] = 1
    return Y_new

def to_rating(Y):
    return Y.argmax(axis=0)
    
class RBM_rec:
    def __init__(self, n_user, n_movie, J = 100, max_rating = 5):
        """
        J: n_hidden
        max_rating: 5 star
        """
        self.n_user,self.n_movie = n_user,n_movie
        self.J = J
        self.max_rating = max_rating
        self._init_W()
        
    def _init_W(self):
        #self.W = np.random.normal(0,0.01,(self.max_rating,self.J,self.n_movie))
        #self.h_bias = np.random.normal(0,0.01,(self.J,))
        #self.v_bias = np.random.normal(0,0.01,(self.max_rating,self.n_movie))
        self.W = np.ones((self.max_rating,self.J,self.n_movie))*0.01
        self.h_bias = np.ones((self.n_user,self.J))*0.01
        self.v_bias = np.ones((self.max_rating,self.n_movie))*0.01
        self.delta_W = np.zeros_like(self.W)
        self.delta_v_bias = np.zeros_like(self.v_bias)
        self.delta_h_bias = np.zeros_like(self.h_bias)
        
        
    def fit(self, Y, batch_size = 100, n_epoch = 30,lr = 0.01, momentum = 0.9, wd = 0.001, isnorm=False,verbose=True):
        for e in xrange(n_epoch):
            err = 0
            for i_batch, (Y_batch,missing_index,beg,last) in enumerate(batch_func(Y, batch_size,self.max_rating)):
                err += self._fit_one_batch(Y_batch,n_cd=1,lr=lr,mom=momentum,wd=wd,missing_index=missing_index,isnorm=isnorm,beg=beg,last=last)
            if verbose:
                Y_pred = self.transform(Y)
                missing_index = np.ones_like(Y_pred,dtype = 'bool')
                index = Y.nonzero()
                missing_index[index[0],index[1]] = False
                Y_pred[missing_index] = 0
                print "epoch: ",e, " rmse: ", np.sqrt(np.mean((np.array(Y.todense())-Y_pred)**2))
                #print "epoch: ",e, "rmse: ", err/Y.shape[0]
        
        
    def _fit_one_batch(self,Y,n_cd=1,lr=0.01,mom=0.9,wd=0.001,missing_index = None, isnorm = False, beg=0,last=0):
        """
        Y matrix should be max_rating*n_batch_size*n_movie
        """
        #positive phase
        h_pos = 0
        for k in xrange(self.max_rating):
             h_pos += np.dot(Y[k],self.W[k].T)
        h_pos = logistic_sigmoid(h_pos + self.h_bias[beg:last])
        pos_cor = np.zeros_like(self.W)
        for k in xrange(self.max_rating):
            pos_cor[k] = np.dot(h_pos.T,Y[k])
        
        #negative phase
        data_neg = np.zeros_like(Y)
        h_neg = h_pos.copy()
        for i in xrange(n_cd):
            for k in xrange(self.max_rating):
                data_neg[k] = np.dot(h_neg,self.W[k]) + self.v_bias[k]
            data_neg = softmax_act(data_neg)
            h_neg = 0
            for k in xrange(self.max_rating): 
                h_neg +=  np.dot(data_neg[k],self.W[k].T)
            h_neg = logistic_sigmoid(h_neg + self.h_bias[beg:last])
        neg_cor = np.zeros_like(self.W)
        for k in xrange(self.max_rating):
            data_neg[k][missing_index] = 0
            neg_cor[k] = np.dot(h_neg.T, data_neg[k])
        
        #update
        lr = float(lr)/Y[0].shape[0]
        for k in xrange(self.max_rating):
            self.delta_W[k] = lr*(pos_cor[k]-neg_cor[k]) + mom*self.delta_W[k] - wd*self.W[k]
            self.delta_v_bias[k] = lr*(Y[k].sum(axis = 0) - data_neg[k].sum(axis = 0)) + mom*self.delta_v_bias[k]
        self.delta_h_bias[beg:last] = lr*(h_pos - h_neg) + mom*self.delta_h_bias[beg:last]
        
        if isnorm:
            for k in xrange(self.max_rating):
                self.delta_W[k] /= np.linalg.norm(self.delta_W[k])
                self.delta_v_bias[k] /= np.linalg.norm(self.delta_v_bias[k])
            self.delta_h_bias[beg:last] /= np.linalg.norm(self.delta_h_bias[beg:last])
        
        self.W += self.delta_W
        self.h_bias[beg:last] += self.delta_h_bias[beg:last]
        self.v_bias += self.delta_v_bias
        return np.sum((Y-data_neg)**2)
        
    def transform(self, Y):
        """
        dont forget to turn ratings from softmax
        """
        Y = to_index(Y.todense(), self.max_rating)
        h_pos = 0
        for k in xrange(self.max_rating):
             h_pos += np.dot(Y[k],self.W[k].T)
        h_pos = logistic_sigmoid(h_pos + self.h_bias)
        for k in xrange(self.max_rating):
            Y[k] = np.dot(h_pos,self.W[k]) + self.v_bias[k]
        Y = softmax_act(Y)
        return to_rating(Y)
        
class RBM_rec_gauss:
    def __init__(self, n_user, n_movie, J = 100, max_rating = 5, Y=None):
        """
        J: n_hidden
        max_rating: 5 star
        """
        self.n_user,self.n_movie = n_user,n_movie
        self.J = J
        self.max_rating = max_rating
        self._init_W(Y=Y)
        
    def _init_W(self,Y=None):
        #self.W = np.random.normal(0,0.01,(self.J,self.n_movie))
        #self.h_bias = np.random.normal(0,0.01,(self.n_user,self.J))
        #self.v_bias = np.random.normal(0,0.01,(self.n_movie,))
        if Y is None:
            self.W = np.ones((self.J,self.n_movie))*0.01
        else:
            _,_,self.W = sp.linalg.svds(Y,k=self.J)
            self.W[self.W<0] = 0
        self.h_bias = np.ones((self.n_user,self.J))*0.01
        self.v_bias = np.ones(self.n_movie)*0.01
        self.delta_W = np.zeros_like(self.W)
        self.delta_v_bias = np.zeros_like(self.v_bias)
        self.delta_h_bias = np.zeros_like(self.h_bias)
        
    def fit_gauss(self, Y, batch_size = 100, n_epoch = 30, n_cd_list = ((1,150),(1,300),(1,500)),lr = 0.01, momentum = 0.9, wd = 0.001, isnorm=False,verbose=True):
        #k = 0        
        #n_cd = n_cd_list[k][0]
        #until = n_cd_list[k][1]
        n_cd = 1        
        for e in xrange(n_epoch):
            err = 0
            #if e == until:
            #    k = k+1
            #    n_cd = n_cd_list[k][0]
            #    until = n_cd_list[k][1]
            for i_batch, (Y_batch,missing_index,beg,last) in enumerate(batch_func_gauss(Y, batch_size)):
                err += self._fit_one_batch_gauss(Y_batch,n_cd=n_cd,lr=lr,mom=momentum,wd=wd,missing_index=missing_index,isnorm=isnorm, beg=beg, last=last)
            if verbose:
                #Y_pred = self.transform_gauss(Y)
                #missing_index = np.ones_like(Y_pred,dtype = 'bool')
                #index = Y.nonzero()
                #missing_index[index[0],index[1]] = False
                #Y_pred[missing_index] = 0
                #print "epoch: ",e, " rmse: ", np.sqrt(np.mean((np.array(Y.todense())-Y_pred)**2))
                print "epoch: ",e, "reconstruction error :", err/Y.shape[0]

        
    def _fit_one_batch_gauss(self,Y,n_cd=1,lr=0.01,mom=0.9,wd=0.001,missing_index = None, isnorm = False,beg=0,last=0):
        """
        Y matrix should be max_rating*n_batch_size*n_movie
        """
        #positive phase
        h_pos = logistic_sigmoid(np.dot(Y,self.W.T) + self.h_bias[beg:last])
        pos_cor = np.dot(h_pos.T,Y)
        
        #negative phase
        h_neg = h_pos.copy()
        for i in xrange(n_cd):
            data_neg = np.dot(h_neg,self.W) + self.v_bias
            h_neg =  logistic_sigmoid(np.dot(data_neg,self.W.T) + self.h_bias[beg:last])
        data_neg[missing_index] = 0
        neg_cor = np.dot(h_neg.T, data_neg)
        
        #update
        lr = float(lr)
        self.delta_W = (pos_cor-neg_cor) #+ mom*self.delta_W - wd*self.W
        self.delta_v_bias = (Y.sum(axis = 0) - data_neg.sum(axis = 0))# + mom*self.delta_v_bias
        self.delta_h_bias[beg:last] = (h_pos - h_neg)# + mom*self.delta_h_bias[beg:last]
        
        if isnorm:
            self.delta_W /= 1*np.linalg.norm(self.delta_W)
            self.delta_v_bias /= 1*np.linalg.norm(self.delta_v_bias)
            #self.delta_h_bias[beg:last] /= np.sqrt(np.sum(self.delta_h_bias[beg:last]**2,axis=1))[:,np.newaxis]
            self.delta_h_bias[beg:last] *=1           
            
        self.W = self.W + lr*self.delta_W - wd*self.W + mom*self.delta_W
        self.h_bias[beg:last] = self.h_bias[beg:last] + lr*self.delta_h_bias[beg:last] + mom*self.delta_h_bias[beg:last]
        self.v_bias = self.v_bias + lr*self.delta_v_bias + mom*self.delta_v_bias
        
        self.W[self.W<0] = 0
        self.h_bias[self.h_bias<0] = 0
        self.v_bias[self.v_bias<0] = 0
        return np.sum((Y-data_neg)**2)
        
    def transform_gauss(self, Y):
        """
        dont forget to turn ratings from softmax
        """
        Y = np.array(Y.todense())
        for i in xrange(100):
            h_pos = logistic_sigmoid(np.dot(Y,self.W.T) + self.h_bias)
            Y = np.dot(h_pos,self.W) + self.v_bias
        return Y
        
if __name__ == '__main__':
    from movie_rec_100k import Recommender
    def test_rbm():
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.base'
        rank = 100
        recommender = Recommender()
        V_train,n_user,n_movie,_ = recommender.read_triple(file_path)
        X_train = recommender.read_txt(V_train,n_user=n_user,n_movie=n_movie,direct = False)
        X_train = X_train.tocsr()
        rbm = RBM_rec(n_user, n_movie,J=rank, max_rating = 5)
        rbm.fit(X_train,batch_size=100,n_epoch=30,
                lr = 0.01, momentum = 0.0, wd = 0, isnorm=True,verbose=True)
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.test'
        V_test,n_user,n_movie,_ = recommender.read_triple(file_path)
        y_pred = rbm.transform(X_train)
        X_test = np.array(V_test)
        err = y_pred[X_test[:,0]-1,X_test[:,1]-1] - X_test[:,-1]
        print "rmse: ", np.sqrt(np.mean(err**2))
        
    def test_rbm_gauss():
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.base'
        rank = 100
        recommender = Recommender()
        V_train,n_user,n_movie,_ = recommender.read_triple(file_path)
        X_train = recommender.read_txt(V_train,n_user=n_user,n_movie=n_movie,direct = False)
        X_train = X_train.tocsr()
        rbm = RBM_rec_gauss(n_user, n_movie,J=rank, max_rating = 5, Y=X_train)
        rbm.fit_gauss(X_train,batch_size=300,n_epoch=500,
                lr = 0.5, momentum = 0.2, wd = 1e-4, isnorm=True,verbose=True)
        file_path = r'C:\Users\daredavil\Documents\movielens\ml-100k\ua.test'
        V_test,n_user,n_movie,_ = recommender.read_triple(file_path)
        y_pred = rbm.transform_gauss(X_train)
        X_test = np.array(V_test)
        err = y_pred[X_test[:,0]-1,X_test[:,1]-1] - X_test[:,-1]
        print "rmse: ", np.sqrt(np.mean(err**2))
        
    test_rbm_gauss()
        
        